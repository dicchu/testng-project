package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EnquiryMemberPage {

    private WebDriver driver;
    private WebDriverWait wait;

    private final By pieTaxButton = By.cssSelector("button[data-nefi-id='TB.PIETAX']");
    private final By pirField = By.cssSelector("input[data-nefi-id='.200588']");

    public EnquiryMemberPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }

    public void clickPieTaxButton() {
        wait.until(ExpectedConditions.elementToBeClickable(pieTaxButton));
        driver.findElement(pieTaxButton).click();
    }

    public boolean verifyPirToBe(String pir) {
        wait.until(ExpectedConditions.elementToBeClickable(pirField));
        System.out.println("PIR = " + driver.findElement(pirField).getAttribute("value"));
        return pir.equals(driver.findElement(pirField).getAttribute("value"));
    }
}
