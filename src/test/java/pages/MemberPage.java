package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MemberPage {

    private WebDriver driver;
    private WebDriverWait wait;

    private final By breadCrumbs = By.cssSelector("a[class='d-inline-flex']");
    private final By menuSearchField = By.id("menuSearch");

    public MemberPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }

    public String getMemberBreadCrumb() {
        return driver.findElements(breadCrumbs).get(1).getText();
    }

    public void searchAndSelectMacro(String macro) {
        driver.findElement(menuSearchField).sendKeys(macro);
        driver.findElement(By.linkText(macro)).click();
    }
}
