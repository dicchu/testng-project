package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Homepage {

    private WebDriver driver;
    private WebDriverWait wait;

    private final By searchField = By.cssSelector("input[name='SearchString1']");
    private final By searchButton = By.cssSelector("button[id='searchSubmitButton']");
    private final By searchResults = By.cssSelector("a[class*='list-group-item'] div[class='d-flex']");

    public Homepage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
    }

    public void populateSearchField(String searchItem) {
        driver.findElement(searchField).sendKeys(searchItem);
    }

    public void clickSearchButton() {
        driver.findElement(searchButton).click();
    }

    public Boolean checkSearchResult() {
        wait.until(ExpectedConditions.elementToBeClickable(searchResults));
        return driver.findElements(searchResults).get(0).isDisplayed();
    }

    public void selectLatestMember() {
        wait.until(ExpectedConditions.elementToBeClickable(searchResults));
        driver.findElements(searchResults).get(driver.findElements(searchResults).size() - 1).click();
    }
}
