package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Dashboard {

    private WebDriver driver;

    private final By cardLink = By.cssSelector("a[class*='card-link']");

    public Dashboard(WebDriver driver) {
        this.driver = driver;
    }

    public void clickCardLink() {
        driver.findElement(cardLink).click();
    }
}
