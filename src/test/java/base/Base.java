package base;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {

    public static WebDriver driver;

    private final String driverProperty = "webdriver.chrome.driver";
    private final String driverLocation = "drivers/chromedriver.exe";
    private final String userCredentials = "ampnz%5Csvcksajp:SUP3RM%40N2";
    private final String ksUrl = "https://" + userCredentials + "@www-ig.ksnp.nzwm.nz";

    @Before
    public void setUp() {
        System.setProperty(driverProperty, driverLocation);
        driver = new ChromeDriver();
        driver.get(ksUrl);
        driver.manage().window().maximize();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
