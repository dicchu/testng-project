package steps.search;

import base.Base;
import pages.Dashboard;
import pages.Homepage;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;

import static org.testng.Assert.assertTrue;

public class SearchSteps {

    private final Dashboard dashboard;
    private final Homepage homepage;

    public SearchSteps() {
        Base base = new Base();
        WebDriver driver = base.driver;
        this.dashboard = new Dashboard(driver);
        this.homepage = new Homepage(driver);
    }

    @Given("The member's first name is {string}")
    public void the_member_s_first_name_is(String firstName) {
        dashboard.clickCardLink();
        homepage.populateSearchField(firstName);
    }

    @When("The first name is looked up")
    public void the_first_name_is_looked_up() {
        homepage.clickSearchButton();
    }

    @Then("A list of matching members is displayed")
    public void a_list_of_matching_members_is_displayed() {
        assertTrue(homepage.checkSearchResult());
    }
}
