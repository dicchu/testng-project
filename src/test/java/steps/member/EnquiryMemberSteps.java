package steps.member;

import base.Base;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import pages.EnquiryMemberPage;
import pages.Homepage;
import pages.MemberPage;

import static org.testng.Assert.assertTrue;

public class EnquiryMemberSteps {

    private final Homepage homepage;
    private final MemberPage memberPage;
    private final EnquiryMemberPage enquiryMemberPage;

    public EnquiryMemberSteps() {
        Base base = new Base();
        WebDriver driver = base.driver;
        this.homepage = new Homepage(driver);
        this.memberPage = new MemberPage(driver);
        this.enquiryMemberPage = new EnquiryMemberPage(driver);
    }

    @Given("The latest member is selected")
    public void the_latest_member_is_selected() {
        homepage.selectLatestMember();
    }

    @Given("The {string} macro is opened")
    public void the_macro_is_opened(String macro) {
        memberPage.searchAndSelectMacro(macro);
    }

    @When("The PIE Tax button is clicked")
    public void the_button_is_clicked() {
        enquiryMemberPage.clickPieTaxButton();
    }

    @Then("The PIR Rate is displayed to be {string}")
    public void the_pir_rate_is_displayed_to_be(String pir) {
        assertTrue(enquiryMemberPage.verifyPirToBe(pir));
    }
}
