# src/test/features/enquiryMember/EnquiryMember-PIETax.feature
Feature: AMP KiwiSaver Application Enquiry - Member Functionality

  As a user of the AMP KiwiSaver Application
  I want to display member information in the registry
  So that I can verify the details in the database

  Scenario: Verify member's Prescribed Investor Rate
    Given The member's first name is "Gary-Eleven"
    And The first name is looked up
    And The latest member is selected
    And The "Enquiry - Member" macro is opened
    When The PIE Tax button is clicked
    Then The PIR Rate is displayed to be "10.50"