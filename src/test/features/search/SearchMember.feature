# src/test/features/search/SearchMember.feature
Feature: AMP KiwiSaver Application Search Functionality

  As a user of the AMP KiwiSaver Application
  I want to search for information in the registry
  So that I can create or update details in the database

  Scenario: Member search using first name
    Given The member's first name is "any value"
    When The first name is looked up
    Then A list of matching members is displayed